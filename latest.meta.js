// ==UserScript==
// @name           YouTube Links Cyru55
// @namespace      http://www.smallapple.net/labs/YouTubeLinks/
// @description    Download YouTube videos. Video formats are listed at the top of the watch page. Video links are tagged so that they can be downloaded easily.
// @author         Ng Hun Yang
// @include        http://*.youtube.com/*
// @include        http://youtube.com/*
// @include        https://*.youtube.com/*
// @include        https://youtube.com/*
// @match          *://*.youtube.com/*
// @match          *://*.googlevideo.com/*
// @match          *://s.ytimg.com/yts/jsbin/*
// @unwrap         x
// @sandbox        raw
// @grant          unsafeWindow
// @grant          GM_xmlhttpRequest
// @grant          GM.xmlHttpRequest
// @connect        googlevideo.com
// @connect        s.ytimg.com
// @version        2.47.6
// ==/UserScript==
